<?php
//include("../lib/mycontroller.php");
require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
$myController = new MyController();
$adminInfo  = $myController->checkLogin(false);

//如果有账号信息，则跳到后台首页
if(!empty($adminInfo))
    //header("Location: /views/admin.php");
    $myController->redirect('/admin/index');
?>
<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <title>跑腿小妹后台登录</title>
    <link rel="stylesheet" href="../static/css/base.css"/>
    <link rel="stylesheet" href="../static/css/login.css"/>
</head>
<body>
<div class="wrap">
    <div class="left">
        <div class="arrow"></div>
        <div class="logo">
            <a href=""><img src="../static/images/login_logo.png" alt=""/></a>
        </div>
        <div class="copyright">
            <p></p>

            <p></p>
        </div>
    </div>
    <div class="right">
        <div class="login_form">
            <div class="submit_warn">
                <p><b></b><span>X</span></p>
            </div>
            <form class="admin_login" action="">
                <span class="login_input">
                    <input type="text" class="name" placeholder="请输入用户名" />
                    <input type="password" class="pwd" placeholder="请输入密码"/>
                </span>
                <span class="login_submit"><input type="submit" value="" class="submit"/></span>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="../static/js/jquery.js"></script>
<script type="text/javascript" src="../static/js/jquery.placeholder.js"></script>
<script>
	$(document).ready(function () {
		$('input, textarea').placeholder();
		$("form.admin_login").on("submit", function (event) {
			event.preventDefault();
			var account = $.trim($(this).find(".name").val());
			var password = $.trim($(this).find(".pwd").val());
			if (account.length < 4 || password.length < 6) {
				$(".submit_warn").find("p").show().find("b").html('用户名或者密码错误');
				return false;
			}else{
				$.ajax({
					url: '/admin/logon',
					type: "POST",
					dataType: "json",
					data: {'aAccount': account, 'aPassword': password},
					success: function (data) {
						if (data.status) {
							window.location.href = "/views/admin.php";
						}else{
							$(".submit_warn").find("p").show().find("b").html(data.msg);
						}
					}
				});
			}
		});
		$(".submit_warn>p>span").on("click", function () {
			$(this).closest("p").hide();
		});
	})
</script>
</body>
</html>
