<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>节目列表</title>
    <script type="text/javascript" src="../static/js/jquery.js"></script>
</head>
<body>
<div>
    <table border="1" id="myTb">
        <tr>
            <td>节目名称</td>
            <td>节目类型</td>
            <td>演出团队</td>
            <td>演出者</td>
        </tr>
    </table>
</div>
</body>
</html>
<script>
$(function(){
    $.getJSON("/MeetingController.php?m=getAll", function(json){
        var resData = json.data;
        var tbBody = '';
        $.each(resData, function(i, n) {
            tbBody += "<tr>" +
                "<td>" + n.mt_name + "</td>" +
                "<td>" + n.mt_type + "</td>" +
                "<td>" + n.mt_performer_from + "</td>" +
                "<td>"+n.mt_performer+"</td>" +
                "</tr>";
            $("#myTb").append(tbBody);
        });
    });
});
</script>