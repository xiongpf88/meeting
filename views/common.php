<?php
require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
$myController = new MyController();
$adminInfo  = $myController->checkLogin();
if(!$adminInfo){
	//$myController->redirect('/admin/index');
}
?>
<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="../static/css/base.css"/>
    <link rel="stylesheet" href="../static/css/common.css"/>
    <script type="text/javascript" src="../static/js/jquery.js"></script>
    <script type="text/javascript" src="../static/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="../static/js/config.js"></script>
	<script type="text/javascript">
		$(document).ready(function (){
			var menu = 	$.cookie('menu'); 
			if(menu){
				menu = '.'+menu;
				var me = $(menu).closest(".nav_menu");
				me.addClass("nav_on").siblings(".nav_menu").removeClass("nav_on");
				me.find("ul li").removeClass("curr");
				var i = $.cookie('i');
				if(i){
					var link = me.find("li").eq(i);
					link.addClass("curr").siblings("li").removeClass("curr");
				}
			}
			
			//退出
			$('.logout').on('click',function (){
				$.ajax({
					url:'/admin/logout',
					type:'post',
					dataType:'json',
					beforeSend:function (){},
					success:function (data){
						alert(data.msg);
						if(data.status){
							window.location.href='/admin/logon';
						}
					}
				});
				return false;
			});

			//左侧菜单点击事件
			$(".nav_menu>a").on("click", function () {
				var me = $(this).closest(".nav_menu");
				me.addClass("nav_on").siblings(".nav_menu").removeClass("nav_on");
				me.find("ul li").removeClass("curr");
			});
			$(".nav_menu ul li>a").on("click", function () {
				var me = $(this).closest("li");
				me.addClass("curr").siblings("li").removeClass("curr");
				var menu = $(this).closest("ul").attr("class");
				$.cookie('menu',menu,{path:"/"});
				var i = me.index();
				$.cookie('i',i,{path:"/"});
			});    
		});
	</script>
</head>
<body>
<!-- 顶部 -->
<div class="top">
    <div class="logo">
        <a href="/views/admin.php"><img src="../static/images/backstage_logo.png" alt=""/></a>
    </div>
    <div class="account">
        <ul>
            <li class="user"><b>用户名：</b><a href=""><?php echo $adminInfo['a_account'];?></a></li>
            <li><b>用户身份：</b><a href=""><?php echo $adminInfo['a_super_admin']==1?'超级管理员':'普通管理员';?></a></li>
            <li><a href="" class="logout">退出</a></li>
        </ul>
    </div>
</div>
<!-- 左侧导航 -->
<div class="left">
    <div class="nav_title">导航菜单</div>
    <div class="nav">
        <div class="nav_menu">
            <a href="#">节目管理</a>
            <ul class="user_suggestion">
                <li> <a href="/meeting/myitems">节目列表</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 面包屑导航 -->
<div class="bread_nav">
    <div class="nav">
<!--        <span>当前位置：</span><a href="">home > 用户建议管理 > 用户建议列表 </a>-->
    </div>
    <div class="info">
        <a href=""></a>
    </div>
    <div class="menu">
        <a class="drop" href="">Drop Menu</a>
<!--        <ul>-->
<!--            <li class="first"><a href="">First</a></li>-->
<!--            <li><a href="">Second</a></li>-->
<!--            <li class="last"><a href="">Third</a></li>-->
<!--        </ul>-->
    </div>

</div>

