<?php
include("common.php");
?>
<title>节目单列表</title>
<link rel="stylesheet" href="../static/css/suggestion.css"/>
<script type="text/javascript" src="../static/js/My97DatePicker/WdatePicker.js"></script>
<div class="toggle_switch">
    <a href="#" class="open">弹出</a> <a href="#" class="close">收起</a>
</div>
<div class="toggle_area" data="">
    <div class="filter_search">
        <form class="filter_domain" action="">
            <span>条件搜索：</span>
            <input type="submit" class="search_submit" value="搜索" />
            <input type="reset" value="清空" class="">
        </form>
    </div>


</div>
<div class="common">
    <div class="container">
        <h2>用户建议列表</h2>
        <div class="domain_list">
            <div class="list_title">
                <span class="sort_id">序号</span>
                <span class="user_open_id">节目名称</span>
                <span class="device_id">类型</span>
                <span class="device_code">排序</span>
                <span class="suggest_content">表演团队</span>
                <span class="suggest_time">演出者</span>
                <span class="a">背景图</span>
                <span class="b">音乐</span>
                <span class="c">链接URL</span>
            </div>
            <ul></ul>
            <div class="page_count"></div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        /*弹出收起层点击事件*/
        $(".toggle_switch a.close").on("click", function(event) {
            event.preventDefault();
            $(".toggle_area").hide();
            $(".common").css("top", "101px");
            $(".toggle_switch").css("top", "102px").find("a.close").hide().siblings("a").css("display", "block");
        });
        $(".toggle_switch a.open").on("click", function(event) {
            event.preventDefault();
            $(".toggle_area").show();
            $(".common").css("top", "230px");
            $(".toggle_switch").css("top", "206px").find("a.open").hide().siblings("a").css("display", "block");
        });

        function resize(){
            var width=$(window).width();
            var cur = $(".filter_domain");
            if(width<1373){
                cur.children().not("div").slice(5).wrapAll("<div class='searchInput' ></div>");
                cur.find("div").css({"margin-top": "10px", "margin-left": "64px"});
            }else{
                cur.find("div").children().unwrap();
            }
        }
        resize();
        $(window).resize(function(){
            resize();
        });
        /*时间选择器，开始*/
        $('#begin_time').click(function (){
            WdatePicker({
                isShowClear:false,
                dateFmt:'yyyy-MM-dd'
            });
        });
        /*时间选择器，结束*/
        $('#end_time').click(function (){
            WdatePicker({
                isShowClear:false,
                dateFmt:'yyyy-MM-dd'
            });
        });

        //初始查询
        conditionSreach();


        //条件查询
        $('.filter_domain').submit(function(event) {
            event.preventDefault();
            conditionSreach();
        });

        //点击页码翻页
        $('.page_count').on('click', 'a', function(event) {
            event.preventDefault();
            var cur = $(this);
            var pageNum = $.trim(cur.attr('rel'));
            conditionSreach(pageNum);
        });

        /**
         * 条件查询
         *
         * @param pageNum       当前页
         */
        function conditionSreach(pageNum) {
            var cur = $(".filter_domain");
            var page = pageNum ? pageNum : 1;
            //变量一定要成对,不能只写send={beginTime}火狐没问题，谷歌会出错
            var send = {'pageNum': page};
            $.ajax({
                url: '/meeting/getItemsList',
                type: 'post',
                dataType: 'json',
                data: send,
                beforeSend: function() {
                    $(".info").html("load......").fadeIn("fast");
                },
                success: function(data) {
                    if (data.status) {
                        total = data.data.total;
                        totalPage = data.data.totalPage;
                        curPage = page;
                        var num = data.data.beginOffset;
                        var str = '';
                        var suggestionList = data.data.result;
                        $.each(suggestionList,function(index,result){
                            num++;
                            str += '<li>';
                            str += '<span class="sort_id">' + num + '</span>';
                            str += '<span class="user_open_id">'+result.mt_name+'</span>';
                            str += '<span class="device_id">'+result.mt_type+'</span>';
                            str += '<span class="device_code">'+result.mt_sort+'</span>';
                            str += '<span class="suggest_content">'+ result.mt_performer_from +'</span>';
                            str += '<span class="suggest_time">'+result.mt_performer+'</span>';
                            str += '<span class="a">'+result.mt_image+'</span>';
                            str += '<span class="b">'+result.mt_music+'</span>';
                            str += '<span class="c">'+result.mt_url+'</span>';
                            str += '</span></li>';
                        });
                        $('.domain_list ul').html(str);
                        $(".info").fadeOut("fast");
                    } else {
                        $(".info").html(data.msg)
                        $('.domain_list ul').html('<li><span>' + data.msg + '</span></li>');
                        total = 0;
                        totalPage = 1;
                        curPage = 1;
                    }
                },
                complete: function() {
                    getPageBar(1, 9, 1);
                }
            });
        }

        //移入显示内容详情
        $('.domain_list ul').on('mouseover','.suggest_content',function (){
            if($(this).find('.detail').html() != ''){
                $(this).find('.detail').show();
            }
        });
        //移出隐藏内容详情
        $('.domain_list ul').on('mouseout','.suggest_content',function (){
            if($(this).find('.detail').html() != ''){
                $(this).find('.detail').hide();
            }
        });

        //截断字符串
        function cutStr (str, len)
        {
            if(str.length > len){
                str = str.substr(0, len) + '...';
            }
            return str;
        }
    });

</script>
</body>
</html>
