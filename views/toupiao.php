<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta http-equiv="Access-Control-Allow-Origin" content="">
    <script src="../static/js/jquery-1.11.3.min.js"></script>
    <script src="../static/js/highcharts.js"></script>
    <script src="../static/js/toupiao.js"></script>
    <script type="text/javascript" src="../static/js/lrtk.js"></script>

    <style>
        text,tspan{
            font-size: 20px !important;
        }
        .wrapper {
            margin:50px auto;
            width:665px;
            font-size:12px;
            display: none;
        }
        .autosize {
            font-size:0;
            background: #000;
            display: table;
        }
    </style>
</head>
<body>
<?php require 'nav.php' ?>
<button id="start" style="float: right;">投票计时</button>
<button id="co" style="float: right;">继续</button>
<div class="wrapper">
    <div id="led2" class="autosize"></div>
</div>
    <div id="container" style="min-width:400px;margin-top:100px ;height:600px"></div>
<script type="text/javascript" src="../static/js/jquery.uled.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var l2 = new uLED({
            id : "led2",
            type : "countdown",
            format : "hh:mm",
            color : "#f0a",
            bgcolor : "#222",
            size : 22,
            rounded : 4
        });
    });
</script>
</body>
</html>