<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=0">
<title>节目投票</title>
<link rel="stylesheet" href="../static/css/weui.min.css"/>
    <script src="../static/js/jquery.js"></script>
</head>
<body>

<div class="weui-cells__title">您喜欢哪3个节目,每桌只能投一次</div>
<div class="weui-cells weui-cells_checkbox">
    <?php foreach($mtList as $key => $value){ ?>
    <label class="weui-cell weui-check__label">
        <div class="weui-cell__hd">
            <input type="checkbox" class="weui-check" name="checkbox1" value="<?php echo($value['id']);?>">
            <i class="weui-icon-checked"></i>
        </div>
        <div class="weui-cell__bd">
            <p><?php echo $value['mt_name'] ?></p>
        </div>
    </label>
    <?php }?>

    <div class="weui-cells__title">请输入桌号:</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <input class="weui-input" type="number" id="tab_number" placeholder="请输入">
            </div>
        </div>
    </div>
    <a href="javascript:void(0);" class="weui-cell weui-cell_link">
        <a class="weui-btn weui-btn_primary" href="javascript:voiceSumbit();" id="showTooltips">投票</a>
    </a>
</div>

</body>
</html>
<script>
    $(function () {

    })

    function voiceSumbit(){
        var items = [];
        $('input:checkbox:checked').each(function() {
            items.push($(this).val());
        });

        if(items.length <3 ){
            alert('说好的投3个节目,在选选吧!');
            return;
        }

        if(items.length > 3){
            alert('最多只能选3个节目!');
            return;
        }

        var tab_number = $('#tab_number').val();
        if(tab_number == ''){
            alert('您是哪桌的,请填写!');
            return;
        }

        if(tab_number > 30){
            alert('这次答谢宴只有30桌!');
            return;
        }

        //拼接字符串
        var itemslist = '';
        items.forEach(function (e) {
            itemslist += e + ',';
        });

        itemslist = itemslist.substring(0,itemslist.length-1);

        $.post('/vote/sbtvoice',{tab_number:tab_number, items:itemslist},function (data) {
            if(data.status){
                alert(data.msg);
            }else{
                alert(data.msg);
            }
        },'json');
        console.log(items);
    }
</script>
