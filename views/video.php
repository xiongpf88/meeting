<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<title>HTML5视频播放器特效 | 超简洁的h5视频播放器| 网页特效库</title>
<meta name="keywords" content="SVG特效, 手机微信网站特效, css3动画, html5特效, 网页特效" />
<link rel="stylesheet" type="text/css" href="../static/css/reset.css"/>
<link rel="stylesheet" href="../static/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../static/css/willesPlay.css"/>
<script src="../static/js/jquery-1.11.3.min.js" type="text/javascript" charset="utf-8"></script>
<script src="../static/js/willesPlay.js" type="text/javascript" charset="utf-8"></script>
	<style>
		/*html { overflow: hidden; }*/
	</style>
</head>
<body>
<?php require 'nav.php' ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="willesPlay">
	<div class="playHeader">
		<div class="videoName" style="color:#000">Tara - 懂的那份感觉</div>
	</div>
	<div class="playContent">
		<div class="turnoff">
			<ul>
				<li><a href="javascript:;" title="喜欢" class="glyphicon glyphicon-heart-empty"></a></li>
				<li><a href="javascript:;" title="关灯" class="btnLight on glyphicon glyphicon-sunglasses"></a></li>
				<li><a href="javascript:;" title="分享" class="glyphicon glyphicon-share"></a></li>
			</ul>
		</div>
		<video width="100%" height="100%" id="playVideo" src="../static/shanghai.mp4"><!--
			<source id="videoSrc" src="../static/shanghai.mp4" type="video/mp4">
			当前浏览器不支持 video直接播放，点击这里下载视频： <a href="/">下载视频</a>-->
		</video>
		<div class="playTip glyphicon glyphicon-play"></div>
	</div>
	<div class="playControll">
		<div class="playPause playIcon"></div>
		<div class="timebar">
			<span class="currentTime">0:00:00</span>
			<div class="progress">
				<div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
				</div>
			<span class="duration">0:00:00</span>
		</div>
		<div class="otherControl">
			<span class="volume glyphicon glyphicon-volume-down"></span>
			<span class="fullScreen glyphicon glyphicon-fullscreen"></span>
			<div class="volumeBar">
					<div class="volumewrap">
						<div class="progress">
						<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 8px;height: 40%;"></div>
					</div>
						</div>
				</div>
		</div>
	</div>
</div>

		</div>
	</div>
</div>
<script>

	var videoSrc = document.getElementById('playVideo');
	function GetQueryString(name)
	{
		var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if(r!=null)return  unescape(r[2]); return null;
	}
	var get = GetQueryString('video');
	if(get != null){
		if(get == 'shang'){
			videoSrc.setAttribute('src','../static/shanghai.MP4');
			document.getElementsByClassName('videoName')[0].innerHTML='上海分公司';
		}else if(get == 'ning'){
			videoSrc.setAttribute('src','../static/ningbo.mp4');
			document.getElementsByClassName('videoName')[0].innerHTML='宁波分公司';
		}else if(get == 'shen'){
			videoSrc.setAttribute('src','../static/shenzhun.mp4');
			document.getElementsByClassName('videoName')[0].innerHTML='深圳分公司';
		}else if(get == 'qing'){
			videoSrc.setAttribute('src','../static/qingdao.mp4');
			document.getElementsByClassName('videoName')[0].innerHTML='青岛分公司';
		}
	}
</script>
</body>
</html>
