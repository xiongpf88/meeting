<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <title>抽奖活动</title>
  <meta name="Generator" content="EditPlus">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">


<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Content-Language" content="zh-CN"/>
<title>抽奖活动</title>
<style type="text/css">
body {padding-top:100px;font:12px "\5B8B\4F53",sans-serif;text-align:center;}
.result_box {margin:100px auto 0;width:900px;padding:100px 0;text-align:center;border:3px solid #40AA53;background:#efe;
 color: #f00;font-size: 50px}
.result_box #oknum {width:700px;color:#cc0000;font-size:50pt;font-family:Verdana;text-align:center;border:none;background:#efe;}
.button_box {margin:50px 0 0 0;}
.button_box .btn {cursor:pointer;padding:0 30px;margin:0 10px;color:#555;font-family:"\5B8B\4F53",sans-serif;font-size:40px;}
input{
 border:none;
 background: none;
 line-height:100px;
 width:160px;
 color:red;
 text-align: center;
 font-size:40px;
}
</style>
</head>
<body>

<?php require 'nav.php' ?>
<div id="text" style="margin-top: 50px;font-size: 50px">幸运奖</div>
<div class="result_box">
 抽奖结果
</div>
<div class="button_box">
 <button class="btn" onclick="start()" accesskey="s">开始(<U>S</U>)</button>
 <button class="btn" onclick="ok()" accesskey="o">停止(<U>O</U>)</button>
</div>
<?php



?>
<script type="text/javascript">

 var alldata1;
 var timer;
 var node;
 var number;
 var par = document.getElementsByClassName("result_box")[0];
 var obj = {};//中奖者名单
 var string = [];//参加者名单
 var stringId = [];//参加者名单ID
 //抽奖数据，以英文逗号分隔

 $.ajax({
  type:"get",
  async:false,
  url:"/namelist/getName",
  dataType:"json",
  success:function (str) {
       var data = str.data;
       for(var i=0;i<data.length;i++){
        string[data[i].ID]=data[i].NAME;
        stringId.push(data[i].ID);
       }
       alldata1 = stringId.join(',');

   },
  error:function (xhr,status,error) {
       console.log(xhr);
       console.log(status);
       console.log(error);
  }
 });


 var alldata = alldata1;
 var alldataarr = alldata.split(",");
 var num = alldataarr.length-1;
 function change(){
       obj.array = {};
       var input = document.getElementsByTagName('input');
       for(var i=0;i<input.length;i++){
        var sui = GetRnd(0,num);
        document.getElementById("k"+i).value = string[alldataarr[sui]];
        obj.array[alldataarr[sui]]=document.getElementById("k"+i).value;
        newdata(alldataarr[sui]);
  }
alldata=alldata1;
 }
 function start(){
      par.innerHTML = '';
      for(var i=0;i<number;i++){
       node = document.createElement("input");
       node.setAttribute("id","k"+i);
       par.appendChild(node);
      }


  clearInterval(timer);
  timer = setInterval('change()',46); //随机数据变换速度，越小变换的越快
 }
 function ok(){
  clearInterval(timer);
  if(confirm('确认提交')){
   obj.status = myurl;
   $.post('/namelist/check',obj,function (data) {
    alert('提交成功');
    console.log(data);
   });
  }
 }

 function newdata(str){
  alldata = alldata.replace(str,"").replace(",,",",");
   // 去掉前置，最末尾的,
   if (alldata.substr(0,1)==",")
   {
   alldata = alldata.substr(1,alldata.length);
   }
   if (alldata.substr(alldata.length-1,1)==",")
   {
   alldata = alldata.substring(0,alldata.length-1);
   }
   alldataarr = alldata.split(",");
   num = alldataarr.length-1;
 }

 function GetRnd(min,max){
  return parseInt(Math.random()*(max-min+1));
 }

 function GetQueryString(name)
 {
  var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if(r!=null)return  unescape(r[2]); return null;
 }

 var text = document.getElementById("text");
 var myurl=GetQueryString("j");//获取get参数值
 if(myurl !=null && myurl.toString().length>=1) {
//  console.log(myurl);
   if(myurl == '5'){
     text.innerHTML = '幸运奖 10名';
    number = 10;
   }else if(myurl == '3'){
    text.innerHTML = '三等奖 5名';
    number = 5;
   }else if(myurl == '2'){
    text.innerHTML = '二等奖 3名';
    number = 3;
   }else if(myurl == '1'){
    text.innerHTML = '一等奖 1名';
    number = 1;
   }else if(myurl == '4'){
    text.innerHTML = '神秘大奖 1名';
    number = 1;
   }
 }



</script>
</body>
</html>
