<link rel="stylesheet" href="../static/css/stylefan.css">

<script src="../static/js/jquery-1.11.3.min.js"></script>

<div class="menuHolder">
    <div class="menuWindow">
        <ul class="p1">
            <li class="s1"><a href="#url"></a>
                <ul class="p2">
                    <li class="s2"><a href="/index" ><span>首页</span></a></li>
                    <li class="s2"><a href="#url"><span>口岸视频</span></a>
                        <ul class="p3 a4">
                            <li><a href="/index/video?video=shang">上海</></li>
                            <li><a href="/index/video?video=ning">宁波</a></li>
                            <li><a href="/index/video?video=shen">深圳</a></li>
                            <li><a href="/index/video?video=qing">青岛</a></li>
                        </ul>
                    </li>
                    <li class="s2"><a href="#url"><span>节目</span></a>
                        <ul class="p3 jiemu">
                            <li><a href="/index/hongbao" >发红包</a></li>
                        </ul>
                    </li>
                    <li class="s2"><a href="#url"><span>颁奖</span></a>
                        <ul class="p3 a3">
                            <li><a href="/index/zhongtian?get=you" >优秀员工</a></li>
                            <li><a href="/index/zhongtian?get=wei" >未来之星</a></li>
                            <li><a href="/index/zhongtian?get=da" >达标奖</a></li>
                        </ul>
                    </li>
                    <li class="s2"><a href="#url"><span>节目投票</span></a>
                        <ul class="p3 a2">
                            <li><a href="/index/pgmlist" >节目单</a></li>
                            <li><a href="/index/toupiao" >投票结果</a></li>
                        </ul>
                    </li>
                    <li class="s2 b6"><a href="#url" ><span>抽奖</span></a>
                        <ul class="p3 a5">
                            <li><a href="/index/cj?j=5" >幸运奖</a></li>
                            <li><a href="/index/cj?j=3" >三等奖</a></li>
                            <li><a href="/index/cj?j=2" >二等奖</a></li>
                            <li><a href="/index/cj?j=1" >一等奖</a></li>
                            <li><a href="/index/cj?j=4" >神秘大奖</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<script>
    $(function(){
        var array = [];
        var mt;
        $.ajax({
            type:"post",
            async:false,
            url:"/index/jiemu",
            dataType:"json",
            success:function (str) {
                var string = $.parseJSON(str.data);
                console.log(string.data);
                mt = string.data;
                for(var i=0;i<mt.length;i++){
                    var mtname = mt[i].mt_name;
                    array.push(mtname);
                    var child = '<li><a href="/index/jiemuAudio" >'+mt[i].mt_name+'</a></li>';
                    $('.jiemu').append(child);
                }
            }
        });
        $('.jiemu').attr('class','a'+(array.length+1)+' p3');
    })
</script>


