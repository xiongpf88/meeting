<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
		body{
			width: 100vw;
			height: 100vh;
			background:url('../static/images/you.jpg') no-repeat;
			-webkit-background-size: 100%;
			background-size: 100%;
		}
    </style>
</head>
<body>
<?php require 'nav.php' ?>
<script>
    function GetQueryString(name)
    {
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if(r!=null){
            return  unescape(r[2]);
        } else {
            return null;
        }
    }
    var get = GetQueryString("get");
    var body = document.getElementsByTagName('body')[0];
    if(get == "you"){
        body.style.backgroundImage = "url('../static/images/you.jpg')"
    }else if(get == "da") {
        body.style.backgroundImage = "url('../static/images/jin.jpg')"
    }else if(get == "wei") {
        body.style.backgroundImage = "url('../static/images/wei.jpg')"
    }
</script>
</body>
</html>