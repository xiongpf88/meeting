(function($) {
	var $tiles = $('#tiles'),
		$handler = $('li', $tiles),
		page = 1,
		isLoading = false,
		apiURL = '/searchdoglist.php',
		lastRequestTimestamp = 0,
		$window = $(window),
		$document = $(document);

	var options = {
		autoResize: false,
		container: $('#tiles'),
		offset: 9,
		itemWidth: 300
	};

    //滚动执行事件
	function onScroll(event) {
		if (!isLoading) {
			var closeToBottom = ($window.scrollTop() + $window.height() > $document.height() - 20); //离底端小于20加载数据
			if (closeToBottom) {
				var currentTime = new Date().getTime();
				if (lastRequestTimestamp < currentTime - 1000) {
					lastRequestTimestamp = currentTime;
					loadData();
				}
			}
		}
	};

    //ajax获取寻狗启示列表
	function loadData() {
		isLoading = true;
		$('#loaderCircle').show();

		$.ajax({
            type: 'POST',
			url: apiURL,
			dataType: 'json',
			data: {
				page: page
			},
			success: onLoadData
		});
	};

    //将获取到的图片数据追加到图片后面
	function onLoadData(response) {
		isLoading = false;
		$('#loaderCircle').hide();

        if (response.status) {
            page++;
            var html = '',
                data = response.data,
                i = 0,
                length = data.length,
                image,
                $newImages;

            for (; i < length; i++) {
                image = data[i];
                var image_height = image.wsd_image_height/image.wsd_image_width*300; //使用相对高度，宽度都是300

                html += '<li>';
                html += '<a href="/views/searchdogdetail.php?image=' + image.wsd_image_name + '" >';
                html += '<img src="../static/images/upload/searchdog/' + image.wsd_image_name + '" width="300" height="'+image_height+'">';
                html += '</a>';
                html += '</li>';
            }

            $newImages = $(html);
            $tiles.append($newImages);
            $handler = $($tiles);
            $handler.wookmark(options);
        }
        else {
            $document.off('scroll', onScroll); //解绑事件,不再触发滚动事件
            $document.off('touchend', onScroll);
        }
	};

	$document.on('scroll', onScroll);  //on来绑定事件
    $document.on('touchend', onScroll); //当手指从屏幕上离开的时候触发
	//$document.on('touchstart', onScroll); //当手指触摸屏幕时候触发，即使已经有一个手指放在屏幕上也会触发
	loadData();
})(jQuery);