//分页
function getPageBar(pagetype, showNumLength, showNum) {
    showNum = showNum ? showNum : true;                                                          //是否显示页码
    curPage = curPage < 1 ? 1 : curPage;                                                                //当前页
    totalPage = totalPage < 1 ? 1 : totalPage;                                                          //总共页码
    showNumLength = showNumLength < 1 ? 1 : showNumLength;                          //显示页码长度
    total = total ? total : 0;                                                                                      //总的记录数
    var pageStr = "<span>共" + total + "条</span><span>当前页：" + curPage + "/" + totalPage + "</span>";
    if (totalPage != 1 && curPage != 1) {
        pageStr += "<a href='' val='" + pagetype + "' rel='1'>首页</a><a href='' val='" + pagetype + "' rel='" + (curPage - 1) + "'>上一页</a>";
    } else if(!showNum && curPage==1){
        pageStr += "<span>首页</span>";
    }

    if (showNum) {
        var leftLength = showNumLength - Math.ceil(showNumLength / 2);
        var leftOffset = (curPage - leftLength > 0) ? (curPage - leftLength) : 1;
        if (leftOffset > 1) {
            pageStr += "...";
        }
        if (leftOffset > showNumLength && leftOffset + showNumLength > totalPage) {
            leftOffset = parseInt(totalPage - showNumLength) + 1;
        }
        for (var i = leftOffset; i < leftOffset + showNumLength; i++) {
            if (i == curPage) {
                pageStr += "<span class='curr'>" + i + "</span>";
            } else {
                pageStr += "<a val='" + pagetype + "' href='' rel='" + i + "'>" + i + "</a>";
            }
            if (i == totalPage) {
                break;
            }
        }
        if (parseInt(curPage) + leftLength < totalPage) {
            pageStr += "...";
        }
    }
    if (curPage != totalPage) {
        pageStr += "<a href='' val='" + pagetype + "' rel='" + (parseInt(curPage) + 1) + "'>下一页</a><a val='" + pagetype + "' href='' rel='" + totalPage + "'>尾页</a>";
    }
    $(".page_count").html(pageStr);
}