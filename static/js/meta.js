/**************************************************************
			 _____                    _____                            _____
			/\    \                  /\    \                          /\    \
		   /::\____\                /::\____\                        /::\____\
		  /:::/    /               /:::|    |                        |::|    |
		 /:::/    /               /::::|    |                        |::|    |
		/:::/    /               /:::::|    |                        |::|    |
	   /:::/    /               /:::|::|    |                        |::|    |
	  /:::/    /               /:::/|::|    |                        |::|    |
	 /:::/    /               /:::/ |::|____|______                  |::|____|____________
	/:::/    /               /:::/  |:::::::::\    \                 |::::::::::::::::|   |
   /:::/    /               /:::/   |::::::::::\____\                /::::::::::::::::|   |
  /:::/    /                \::/    /￣￣￣/:::/    /               /:::/￣￣/￣￣￣￣￣￣
 /:::/____/                  \/____/      /:::/    /               /:::/    /
 \:::\    \                              /:::/    /               /:::/    /
  \:::\    \                            /:::/    /               /:::/    /
   \:::\    \                          /:::/    /               /:::/    /
	\:::\    \                        /:::/    /               /:::/    /
	 \:::\    \                      /:::/    /               /:::/    /
	  \:::\____\                    /:::/    /               /:::/    /
	   \::/    /                    \::/    /                \::/    /
		\/____/                      \/____/                  \/____/

 @Name: 	本脚本仅限个人使用及经本人同意者使用
 @Author:   8.惹我
 @Date: 	2015-03-24
 @QQ: 		443119044
 @Note: 	如未经过本人允许用于第三方任何用途，第三方使用者一辈子吃泡面没有面.
		
 *************************************************************/
;!function(agent, scale){
	if(/firefox/.test(agent)) return document.head.innerHTML += '<meta name="viewport" content="width=640, initial-scale='+ scale +', user-scalable=no">';
	return /android/.test(agent) ? document.head.innerHTML += '<meta name="viewport" content="width=640, initial-scale='+ scale +', target-densitydpi=device-dpi">'
								 : document.head.innerHTML += '<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">';
}(navigator.userAgent.toLowerCase(), parseFloat(window.screen.width)/640);