<?php

require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
require_once(dirname(dirname(__FILE__)) . '/lib/MyLog.php');
require_once(dirname(dirname(__FILE__)) . '/models/meetingmodel.php');


/**
 * Created by IntelliJ IDEA.
 * User: e1
 * Date: 16/11/18
 * Time: 下午5:28
 */
class MeetingController extends MyController
{
    /**
     * MeetingController constructor.构造函数
     */
    public function __construct(){
        parent::__construct();
    }
    
    public function myitems(){
        $this->render('items');
    }

    /**
     * 获取节目列表
     *
     * @return json
     */
    public function getItemsList(){
        $pageNum = $_POST['pageNum'];
        $whereSql = ' where 1=1 ';
        $recordNum = 20; //每页记录数
        $offset = ($pageNum - 1) * $recordNum; //偏移量
        $metModel = new MeetingModel();
        $result = $metModel->getMeeting($offset,$recordNum,$whereSql);
        if(empty($result))
            $this->formatReturn(false,'查询不到结果');
        else{
            $res = array();
            $total = $metModel->getCount($whereSql);
            $res['total'] = empty($total) ? 0 : $total[0]; //记录总数
            $res['beginOffset'] = $offset;  //页面开始偏移量
            $res['totalPage'] = ceil($res['total']  / $recordNum);  //总页数
            $res['result'] = $result;
            $this->formatReturn(true,'菜单列表获取成功',$res);
        }
    }

    /**
     * 获取所有节目列表
     */
    public function getAll(){
        $model = new MeetingModel();
        $result = $model->getAllMeeting();
        $this->formatReturn(true,'',$result);
    }
}
