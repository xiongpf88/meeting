<?php
require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
/**
 * 登录退出相关
 *
 */

class AdminController extends MyController
{	

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index(){
		$myController = new MyController();
		$adminInfo = $myController->checkLogin(false);
		if(!$adminInfo){
			$this->redirect('/admin/login');
		}

		$this->render('admin');
	}

	public function admin(){
		$this->render('admin');
	}
	
	public function login(){
		$this->render('login');
	}

    /**
     * 验证是否传递正确参数
     *
     * @return array
     */
    private function valid(){
        $aAccount = empty($_POST['aAccount'])?'':$_POST['aAccount'];
        $aPassword = empty($_POST['aPassword'])?'':$_POST['aPassword'];
        $res = $this->checkAccount($aAccount);
        if($res==false){
            throw new Exception('用户名格式不正确', 0);
        }
        $res = $this->checkPwd($aPassword);
        if($res==false){
            throw new Exception('密码格式不正确', 0);
        }
        return array('aAccount'=>$aAccount,'aPassword'=>$aPassword);
    }

    /**
     * 登录信息验证post提交地址
     *
     * @return json
     */
	public function logon()
	{
		try
		{
            $res = $this->valid();
			$aAccount = $res['aAccount'];
			$aPassword = $res['aPassword'];
			$aPassword = $this->encryptpwd($aPassword);

			//根据帐号获取帐号信息
            $adminModel = new AdminModel();
			$userInfo = $adminModel->getAdminInfoByAccount($aAccount);
			if(empty($userInfo))
				throw new Exception('用户名不存在', 0);
			//检测登录帐号密码
			if($userInfo['a_pwd'] != $aPassword)
                throw new Exception('密码错误', 0);
			
			//过滤帐号信息,写入redis和session
			unset($userInfo['a_pwd']);
            $this->intoLoginRedis($userInfo);

			$this->formatReturn(TRUE, '登录成功', '');
		}
		catch(Exception $e)
		{	
			$this->formatReturn(false, $e->getMessage(), '');
		}		
	}

    /**
     * md5加密密码
     *
     * @param string $pwd 密码
     * @param string $str 加密的随机字符串
     * @return array
     */
    private function encryptpwd($pwd, $str = '') {
        return md5(md5(md5($pwd) . $str) . $str);
    }

    /**
     * 检测用户名是否符合格式
     * @param unknown $aAccount
     * @return boolean
     */
    public function checkAccount ($aAccount)
    {
        $preg = '/^\w{4,}$/s';  //英数字加下划线串,至少4个
        if(preg_match($preg, $aAccount))
            return TRUE;
        return FALSE;
    }

    /**
     * 检测密码组合是否符合格式
     * @param unknown $uPassword
     * @return boolean
     */
    public function checkPwd ($uPassword)
    {
        $preg = '/^(?!\d+$)(?![a-z]+$)(?![A-Z]+$)(?![-@\!\$\#\%\?_]+$)[\w-@\!\$\#\%\?]{6,32}$/s';
        if(preg_match($preg, $uPassword))
            return TRUE;
        return FALSE;
    }

    /**
	 * 退出
     *
	 * @throws Exception
	 */
	public function logout ()
	{
		try 
		{
			$res = $this->logoutRedis();
			if(empty($res))
				throw new Exception('退出失败！', 0);
			$this->formatReturn(TRUE, '退出成功!', '');
		}
		catch (Exception $e)
		{
			$this->formatReturn(false, $e->getMessage(), '');
		}
	}


}
