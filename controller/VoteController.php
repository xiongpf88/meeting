<?php

require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
require_once(dirname(dirname(__FILE__)) . '/models/meetingmodel.php');
require_once(dirname(dirname(__FILE__)) . '/models/ConfigModel.php');
require(dirname(dirname(__FILE__)) . '/models/voicemodel.php');

class VoteController extends MyController{
  public function test(){
    echo "hello";
  }

  public function index(){
      $meeting = new MeetingModel();
      $mtdata = $meeting->getAllMeeting();
      $data['mtList']=$mtdata;
      $this->render('voice', $data);
  }

  //投票开始：status=1,投票结束：status=0
  public function voiceStart(){
      $status  = $_REQUEST['status'];
      $config = new ConfigModel();
      $config->updateConfig("voice", $status);

      $this->formatReturn(true, '操作成功', '');
  }

  /**
   * 前端ajax投票
   */
  public function sbtvoice(){
      $items = $_REQUEST['items'];
      $tab_number = $_REQUEST['tab_number'];

      $config = new ConfigModel();
      $res = $config->getSysConfig("voice");
      if($res['mt_value']=="0"){
          $this->formatReturn(false, '必须在规定的时间内投票!', '');
          return;
      }

      //判断此桌是否已经投票了
      $voice_model = new VoiceModel();
      $count = $voice_model->getVoice($tab_number);

      if((int)$count>0){
          $this->formatReturn(false, $tab_number.'号桌不能重复投票!', $count);
          return;
      }

      $items = explode(',', $items);
      foreach ($items as $item){
          $voice = new VoiceModel();
          $voice->tab_number = $tab_number;
          $voice->mt_id = $item;
          $voice->openid = '';

          //保存
          $voice->save();
      }

      $this->formatReturn(true, '投票成功,谢谢参与!', '');
  }

  /**
   * 获取投票结果
   */
  public function getVoice(){
      $mtmodel = VoiceModel::model()->getAll();
      $this->formatReturn(true, '', $mtmodel);
  }
}
