<?php

require_once(dirname(dirname(__FILE__)) . '/lib/mycontroller.php');
/**
 * Created by IntelliJ IDEA.
 * User: e1
 * Date: 16/11/25
 * Time: 上午10:02
 */
class IndexController extends MyController
{
    public function index(){
        $this->render('index');
    }

    public function getpar(){
        $id=$_GET['id'];
        echo $id;
    }

    public function cj(){
        $this->render('cj');
    }

    public function hongbao(){
        $this->render('hongbao');
    }

    public function jiemuAudio(){
        $this->render('jiemuAudio');
    }

    public function pgmlist(){
        $this->render('pgmlist');
    }

    public function toupiao(){
        $this->render('toupiao');
    }

    public function video(){
        $this->render('video');
    }

    public function zhongtian(){
        $this->render('zhongtian');
    }

    public function nav(){
        $this->render('nav');
    }

    public function json(){
        $json = file_get_contents("http://192.168.1.251:8112/voice/getVoice");
        echo $this->formatReturn(true,'', $json);
    }

    public function jiemu(){
        $json = file_get_contents("http://192.168.1.251:8112/meeting/getAll");
        echo $this->formatReturn(true,'', $json);
    }
}