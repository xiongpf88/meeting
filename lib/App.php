<?php

/**
 * Created by IntelliJ IDEA.
 * User: e1
 * Date: 16/11/24
 * Time: 下午5:41
 */

//
final class App
{
    public function __construct(){
        $this->autoClass();
    }

    public function autoClass(){
        //require
    }

    public static function run($url){
        $arry = explode('/', $url['path']);
       
        //var_dump($arry);exit;
        
        if(count($arry) == 2){
            if($arry[0] == '' && $arry[1] == ''){
                $controller = 'index';
                $method='index';
            }else if($arry[1] == 'index'){
                $controller = 'index';
                $method='index';
            }else{
                $controller=$arry[1];
                $method='index';
            }
        }else{
            $method=$arry[2];
            $controller=$arry[1];
        }
        $controller=ucfirst($controller).'Controller';
        $c_path='controller/'.ucfirst($controller).'.php';

        require($c_path);
        $controller=new $controller;
        $controller->$method();
    }
}