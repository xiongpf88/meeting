<?php

class ConnectDB{
    private $host;
    private $user;
    private $pwd;
    private $dbName;
    private $conn;

    /**
     * 构造方法连接数据库
     *
     * @param string $host 主机地址
     * @param string $user 用户名
     * @param string $pwd 密码
     * @param string $dbName 数据库名称
     * @return boolean
     */
    public function __construct($host,$user,$pwd,$dbName)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pwd = $pwd;
        $this->dbName = $dbName;
        $this->dbConnect();
    }

    /**
     * 连接数据库
     *
     * @return boolean
     */
    private function dbConnect(){
        if($this->conn && ($this->conn instanceof mysqli) && !false)
        {
            return ;
        }
        $this->conn=new mysqli($this->host,$this->user,$this->pwd,$this->dbName);
        if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }
        $this->conn->set_charset('utf8');
    }

    /**
     * 获取sql语句查询列表数据
     *
     * @param $sql sql语句
     * @return array
     */
    public function getList($sql){
        $query=$this->conn->query($sql);
        if(empty($query))
            return false;
        $result = array();
        while($row=mysqli_fetch_assoc($query)){ //mysql_fetch_assoc:表字段名作为键
            $result[] = $row;
        }
        return $result;
    }

    /**
     * 获取sql语句查询单条数据
     *
     * @param $sql sql语句
     * @return array
     */
    public function getOne($sql){
        $query=$this->conn->query($sql);
        if(empty($query))
            return false;
        return mysqli_fetch_assoc($query);
    }

    public function count($sql){
        $query=$this->conn->query($sql);
        if(empty($query))
            return false;
        return mysqli_num_rows($query);
    }

    /**
     * 插入数据
     *
     * @param $sql sql语句
     * @return boolean
     */
    public function addInfo($sql){
        $query = $this->conn->query($sql);
        return $query;
    }

    /**
     * 更新数据
     *
     * @param $sql sql语句
     * @return boolean
     */
    public function updateInfo($sql){
        $query = $this->conn->query($sql);
        return $query;
    }

    /**
     * 删除数据
     *
     * @param $sql sql语句
     * @return boolean
     */
    public function deleteInfo($sql){
        $query = $this->conn->query($sql);
        return $query;
    }

    /**
     * 关闭数据库连接
     *
     * @return array
     */
    public function close() {
        mysqli_close($this->conn);
    }

}
?>
