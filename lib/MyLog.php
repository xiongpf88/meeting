<?php

/**
 * Class Log
 * pengfei_xiong
 */
class MyLog {

    const URL = "/Users/e1/xpf/test/Log/";
    //const URL = "/var/www/html/logs/";
    /**
     * @param $msg
     */
    static function LogInfo($msg, $tag = null) {

        if (empty ( $msg ))
            return;

        //如果不存在
        if (!is_dir(self::URL)) {
            mkdir(self::URL);
        }

        switch(func_num_args()){
            case 1:
                //日志地址

                $fp = fopen ( self::URL. date ( "Ymd", time () ) . ".log", "a" );
                fwrite ( $fp, date ( "Y-m-d h:i:s", time () ) . "\r\n" );
                fwrite ( $fp, var_export($msg,true) . "\r\n\r\n\r\n ====================================================================================" );
                fclose ( $fp );

                break;
            case 2:
                //日志地址
                $fp = fopen ( self::URL .$tag. date ( "Ymd", time () ) . ".log", "a" );
                fwrite ( $fp, date ( "Y-m-d h:i:s", time () ) . "\r\n" );
                fwrite ( $fp, var_export($msg,true) . "\r\n\r\n\r\n ====================================================================================" );
                fclose ( $fp );

                break;

            default:
                $fp = fopen ( self::URL. date ( "Ymd", time () ) . ".log", "a" );
                fwrite ( $fp, date ( "Y-m-d h:i:s", time () ) . "\r\n" );
                fwrite ( $fp, var_export($msg,true) . "\r\n\r\n\r\n ====================================================================================" );
                fclose ( $fp );
        }

    }
}