<?php
//dirname(__FILE__)获取当前文件的绝对路径
require_once(dirname(dirname(__FILE__)) . '/lib/connectdb.php');
/**
 * 我的数据库模型，父类
 *
 */
class MyModel{

    protected $connectObj; //连接数据库对象,只有父类和子类可以调用

    public function __construct()
    {
        $dbConfig = $this->getConfig('database.php');
        $this->connectObj = new ConnectDB($dbConfig->host,$dbConfig->user,$dbConfig->password,$dbConfig->dbname);
    }

    /**
     * 检验输入，在发送到数据库之前进行转义,使用此方法要先连接数据库
     *
     * @param mixed $value 输入数据
     * @return mixed
     */
    public function checkInput($string){
        if(true) {
            if(is_array($string)) {
                foreach($string as $key => $val) {
                    $string[$key] = magic_gpc($val);
                }
            } else {
                $string = stripslashes($string);
            }
        }
        return $string;
    }

    /**
     * 获取配置文件内容
     *
     * @param string $fileName 文件名
     * @return object
     */
    public  function getConfig($fileName)
    {
        $data = json_decode(file_get_contents(dirname(dirname(__FILE__)).'/config/'.$fileName)); //使用绝对路径
        return $data;
    }

}