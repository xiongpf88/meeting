<?php
/**
 * 我的控制器，父类
 *
 */
require(dirname(__FILE__) . '/Template.php');
require(dirname(dirname(__FILE__)) . '/models/adminmodel.php');

class MyController{

    protected $appId;  //微信appid
    protected $appSecret;  //微信appsecret
    protected $httpHost;   //服务器地址,host要用指定的，否则测试环境会错
    public    $myRedis; //公开的缓存实例
    protected $aIdSessionKey = 'account_id'; //保存aId的session键
    protected $accountRedisPrefix = 'account_info'; //redis保存账号信息前缀

    public function __construct()
    {
        //$this->myRedis = new Redis();
        //$this->myRedis->connect("127.0.0.1","6379");
        $myConfig = $this->getConfig('myconfig.php');
        $this->appId = $myConfig->appId;
        $this->appSecret = $myConfig->appSecret;
        $this->httpHost = $myConfig->httpHost;
        spl_autoload_register(array($this, 'getClassPathByClassName')); //当PHP使用new实例化类,但找不到类文件会调用这个方法spl_autoload_register
        if (!session_id()) session_start(); //开启会话
    }

    public function redirect($url){
        header("Location: ". $url);
    }

    /**
     * 渲染视图
     * @param $view
     */
    public function render($view, $data = array()){
        $template = new Template();
        $template->init($view,$data);
        $template->outPut();
        //require('views/'.$view.'.php');
    }

    /**
     * 登录成功后将信息写入redis缓存和session
     *
     * @param array $userInfo
     * @return boolean
     */
    public function intoLoginRedis($userInfo)
    {
        //记录帐号ID到SESSION
        $_SESSION[$this->aIdSessionKey] = $userInfo['a_id'];

        //帐号信息写入缓存
        $res = $userInfo['a_id']; //缓存半小时
        return $res;
    }

    /**
     * 判断用户是否登录
     *
     * @param boolean $errorProcess 是否处理错误，默认true,如果设置为false，则不做处理，直接返回false;
     * @return boolean|Ambigous <boolean, unknown, unknown>
     */
    public function  checkLogin($errorProcess=TRUE)
    {
        $aId = $this->getSessionAid();
        if(empty($aId))
        {
            $this->errorProcess('未登录或登录已超时，请重新登录！',$errorProcess);
            return FALSE;
        }
        //$adminInfo = unserialize($this->myRedis->get($this->accountRedisPrefix.$aId));
        $adminInfo = null;
        //如果没有管理员账号信息缓存,就重新写缓存
        if(empty($adminInfo)) {
            $adminModel = new AdminModel();
            $adminInfo = $adminModel->getAdminInfoByAid($aId);
            if(empty($adminInfo))
            {
                $this->errorProcess('未登录或登录已超时，请重新登录！',$errorProcess);
                return FALSE;
            }
            unset($adminInfo['a_pwd']);
        }

        //判断账号是否为空
        if(empty($adminInfo['a_account']))
        {
            $this->errorProcess('未登录或登录已超时，请重新登录！',$errorProcess);
            return FALSE;
        }

        //返回帐号信息
        return $adminInfo;
    }

    /**
     * 获取SESSION中的帐号ID号
     *
     * @return boolean|integer
     */
    public function getSessionAid ()
    {
        if(empty($_SESSION[$this->aIdSessionKey]))
            return FALSE;
        $aId = $_SESSION[$this->aIdSessionKey];
        return $aId;
    }


    /**
     * 错误处理
     *
     * @param unknown $errorMsg 错误信息
     * @param boolean $errorProcess 是否处理错误，默认true,如果设置为false，则不做处理，直接返回false;
     */
    protected function errorProcess($errorMsg,$errorProcess)
    {
        if(!$errorProcess)
            return FALSE;
        //判断是否是ajax请求
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest')
        {
            echo $this->formatReturn(FALSE, $errorMsg);
            exit();
        }
        //跳到登录页
        //header("Location: /views/login.php");
        $this->redirect('/admin/login');
    }

    /**
     * 退出登录,清空当前用户session值、redis值
     *
     * @return boolean
     */
    public function logoutRedis()
    {
        $aId = $this->getSessionAid();
        if(empty($aId))
            return TRUE;

        //删除用户ID
        unset($_SESSION[$this->aIdSessionKey]);

        //删除 Redis 中用户信息
        return $this->myRedis->delete($this->accountRedisPrefix.$aId);
    }

    /**
     * 检验输入，在发送到数据库之前进行转义,使用此方法要先连接数据库
     *
     * @param mixed $value 输入数据
     * @return mixed
     */
    public function checkInput($value){
        // 如果有开启magic_quotes_gpc(特殊字符会被加上反斜线)选项，则去除斜杠
        if (get_magic_quotes_gpc())
        {
            $value = stripslashes($value);
        }
        //如果不是数字,则转义 SQL 语句中使用的字符串中的特殊字符',",\等,使用此函数要先连接数据库
        if (!is_numeric($value))
        {
            $value = mysql_real_escape_string($value);
        }
        return $value;
    }

    /**
     * 统一返回格式
     *
     * @param boolean $status 返回状态
     * @param string $msg 错误信息
     * @param array $data 返回数据
     * @return json
     */
    public  function formatReturn($status,$msg,$data=null)
    {
        $status = empty($status)?false:true;
        echo json_encode(array('status' => $status, 'msg' => $msg,'data' =>$data));
    }

    /**
     * 获取配置文件内容
     *
     * @param string $fileName 文件名
     * @return object
     */
    public  function getConfig($fileName)
    {
        //被其他文件调用此方法时dirname(__FILE__)路径还是mycontroller文件的dirname(__FILE__)路径
        $data = json_decode(file_get_contents(dirname(dirname(__FILE__)).'/config/'.$fileName)); //使用绝对路径
        return $data;
    }

    /**
     * 获取当前微时间,用于测试php执行时间
     *
     * @return float
     */
    public function getMicrotime()
    {
        list($msec, $sec) = explode(' ', microtime()); //精确到微妙,返回秒数和当前时间戳,字符串的两部分都是以秒为单位返回的
        return ((float)$msec + (float)$sec);
    }

    /**
     * 静态方法,根据类名加载不同的类文件
     *
     * @return boolean
     */
    public  static function getClassPathByClassName($className)
    {
        $path = '';
        $suffix = strtoupper(substr($className, strlen($className) - 5)); //最后5个字母
        switch($suffix)
        {
            case 'MODEL':
                $path = '/models/';
                break;
            default:
                return true;  //退出整个getClassPathByClassName函数
                break;
        }
        $file = dirname(__FILE__) . $path . strtolower($className) . '.php';
        is_file($file) ? include_once $file : '';  ////include加载模型类
        return true;
    }

}


?>
