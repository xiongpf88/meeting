<?php
/**
 * 管理员模型
 *
 */
require(dirname(dirname(__FILE__)) . '/lib/mymodel.php');

class AdminModel extends MyModel{

    private  $table = 'wq_admin';  //管理员表
    public function __construct()
    {
        parent::__construct ();
    }

    /**
     * 根据管理员ID获取管理员账号信息
     *
     * @param string $aId 管理员ID
     * @return array
     */
    public function getAdminInfoByAid($aId){
        $aId = $this->checkInput($aId);
        $sql = "SELECT * FROM {$this->table} WHERE a_id=$aId";
        $res = $this->connectObj->getList($sql);
        if(!empty($res))
            return $res[0]; //返回第一个记录
        else
            return $res;
    }

    /**
     * 根据管理员账号获取管理员账号信息
     *
     * @param string $aAccount 管理员账号
     * @return array
     */
    public  function getAdminInfoByAccount($aAccount){
        $aAccount = $this->checkInput($aAccount);
        $sql = "SELECT * FROM {$this->table} WHERE a_account='$aAccount'";
        $res = $this->connectObj->getList($sql);
        if(!empty($res))
            return $res[0]; //返回第一个记录
        else
            return $res;
    }

}