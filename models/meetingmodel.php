<?php

    /**
     * Created by IntelliJ IDEA.
     * User: e1
     * Date: 16/11/18
     * Time: 下午5:49
     */
//require(dirname(dirname(__FILE__)) . '/lib/mymodel.php');

class MeetingModel extends MyModel
{
    //节目表
    private  $table = 'mt_meeting';

    public function __construct()
    {
        parent::__construct ();
    }

    public static function model(){
        return new MeetingModel();
    }

    /**
     * 节目分页列表
     * @param $offset
     * @param $recordNum
     * @param $whereSql
     * @return array
     */
    public function getMeeting($offset,$recordNum,$whereSql){
        $sql = "select * from {$this->table} $whereSql order by id asc LIMIT $recordNum OFFSET $offset";
        $res = $this->connectObj->getList($sql);
        return $res;
    }

    /**
     * 获取所有的节目
     * @return array
     */
    public function getAllMeeting(){
        $sql = "select * from {$this->table} ";
        $res = $this->connectObj->getList($sql);
        return $res;
    }

    /**
     * 获取用户建议总记录数
     *
     * @param string $whereSql //where条件的sql语句
     * @return boolean
     */
    public function getCount($whereSql){
        $sql = "select count(*) from {$this->table} $whereSql";
        $res = $this->connectObj->getOne($sql);
        return $res;
    }
}

