<?php

/**
 * Created by IntelliJ IDEA.
 * User: e1
 * Date: 16/11/30
 * Time: 上午11:06
 */
//require(dirname(dirname(__FILE__)) . '/lib/mymodel.php');

class VoiceModel extends MyModel{

    //投票表
    private  $table = 'mt_voice';

    public $id;
    public $mt_id;
    public $openid;
    public $tab_number;

    public function __construct()
    {
        parent::__construct ();
    }

    public static function model(){
        return new VoiceModel();
    }

    public function getAll(){
        $sql = "select * from ".$this->table;
        $res = $this->connectObj->getList($sql);
        return $res;
    }

    //保存
    public function save(){
        $sql = "insert into {$this->table}(mt_id,openid,tab_number)";
        $sql .= " values ('$this->mt_id','$this->openid','$this->tab_number')";
        $res = $this->connectObj->addInfo($sql);
        return $res;
    }

    //根据桌号获取投票记录
    public function getVoice($tab_number){
        $sql = "select * from ".$this->table ." where tab_number=".$tab_number;
        $res = $this->connectObj->count($sql);
        return $res;
    }
}